/* ----------------------------------------------------------------------------
*******************************************************************************
**                                                                           **
**  Unpublished - all rights reserved under the copyright laws of Switzer-   **
**  land and all other countries.                                            **
**                                                                           **
**  Copyright (c) RADICOS Technologies GmbH, Austria                         **
**  Copyright (c) L&V Design, Switzerland                                    **
**  Copyright (c) V-Engineering, Switzerland                                 **
**  All Rights Reserved                                                      **
**  Licensed Material -- Property of Radicos/V-Engineering/L&V DESIGN        **
**                                                                           **
*******************************************************************************
----------------------------------------------------------------------------- */
/**
    @file
    @brief Header file for interp.c
    @version
    @date
    @author
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INTERP_H_INCLUDED
#define __INTERP_H_INCLUDED


/* Includes ------------------------------------------------------------------*/
//#include "stm8l15x.h"
#include "vos.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define SZ_CMDBUF  (80)
#define CMDBUFLEN  SZ_CMDBUF

/* Exported macro ------------------------------------------------------------*/
/* Exported global variables------------------------------------------------- */

/* Exported functions ------------------------------------------------------- */


//---------------------------------------------------------------------------

#define INTERP_MAX_PARAMS   (16)              /* Max nb of arguments for commands. ==16 */

typedef struct {
    uint8     numarg;
    uint8    *argum[INTERP_MAX_PARAMS +1];
} PARAMS_T;

typedef struct CMDTABLETAG
{
    //uint8    *command;
    uint8    command[5];
    //int16_t    (*pfunction)(int,...);
    int16    (*pfunction)(PARAMS_T *pP);
} CMDTABLE_T;

/*
The command parser:
- Input = pointer to command buffer
- Command buffer hold the comman string in the format : Cmd,Arg1,Arg2...<CR>
- return = error code according to errdef.h or listed below
*/
int16 parse ( uint8 * );   // Parser for Maintenance Port


//---------------------------------------------------------------------------

#ifndef EXEC_OK
    #define EXEC_OK      (0)
#endif //EXEC_OK

#ifndef EXEC_WAIT
    #define EXEC_WAIT    (1)
#endif

#ifndef ERR_PARSE
    #define ERR_PARSE    (2)
#endif

#ifndef ERR_ARG
    #define ERR_ARG      (3)
#endif

#ifndef EXEC_EXEC
    #define ERR_EXEC     (4)
#endif

#ifndef NO_ERRCODE
    #define NO_ERRCODE   (255)
#endif



//---------------------------------------------------------------------------
#endif //__INTERP_H_INCLUDED

/******************* (C) COPYRIGHT RADICOS Technologies GmbH   **************/
/******************* (C) COPYRIGHT V-Engineering ****************************/
/******************* (C) COPYRIGHT L&V Design ********* END OF FILE *********/


