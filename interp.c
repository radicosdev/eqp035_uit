/* ----------------------------------------------------------------------------
*******************************************************************************
**                                                                           **
**  Unpublished - all rights reserved under the copyright laws of Switzer-   **
**  land and all other countries.                                            **
**                                                                           **
**  Copyright (c) RADICOS Technologies GmbH, Austria                         **
**  Copyright (c) L&V Design, Switzerland                                    **
**  Copyright (c) V-Engineering, Switzerland                                 **
**  All Rights Reserved                                                      **
**  Licensed Material -- Property of Radicos/V-Engineering/L&V DESIGN        **
**                                                                           **
*******************************************************************************
---------------------------------------------------------------------------- */

/**
    @file
    @brief
        tbd
    @par Purpose of this file
    @par Implementation :
          TBD
    @par Usage :
    @version
    @date
*/

/******************************************************************************
 * @addtogroup ______
 * @{
 *      @addtogroup xxxxxx
 *      @brief
 *      @{
 ******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "vos.h"
#include <string.h>
#include <ctype.h>
#include "interp.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Global variables to be exported , check in xxxx.h -------------------------*/

/* Private variables ---------------------------------------------------------*/
uint8 * cmdp;
	

/* Private function prototypes -----------------------------------------------*/
//int16_t skip_spaces(char *string, int index);
//int strcmpi ( const char *src, const char *dst );

/* Private functions ---------------------------------------------------------*/


//---------------------------------------------------------------------------

extern int16 CmdDisplayVersion    (PARAMS_T *pP);
extern int16 CmdNodeSelect      	(PARAMS_T *pP);
extern int16 CmdDisableAll        (PARAMS_T *pP);

//---------------------------------------------------------------------------
const CMDTABLE_T cmdtable[] =
{
    // Official cmd
    { "VER",   CmdDisplayVersion  },

    // Nodes Select
    { "NS",   	CmdNodeSelect   },
    { "DA",  	CmdDisableAll   },

    { "END_T" , (int16) NULL    }
};



/*----------------------------------------------------------------------*/
/* command line pre-processing & parsing                                */
/* up to 16 parameters supported                                        */
/*                                                                      */
/* returns   EXEC_OK   :exec OK                                         */
/*           EXEC_ERR  :exec error                                      */
/*           PARSE_ERR :parser error                                    */
/*----------------------------------------------------------------------*/

int16 parse(uint8 *cmd_buf)
{

    uint8  error_msg;
    uint8  i;
    uint8  cmd_index;
    uint8  parms_index;
    uint8  exit_flag;
    uint8  end_cmd_flag;
    uint8  more_argum;
    PARAMS_T cmd_parms;
//	uint8 cmdp[128];
	char cmd[128];
	char cmd1[128];
    /* house keeping*/
    parms_index  = 0;
    end_cmd_flag = 0;
    more_argum   = 0;

    strcpy(cmd , cmd_buf);               /* make a working copy*/

    /* special for checkpoint protocol */
    cmd_buf[strlen((char const *)cmd_buf)-1] = '\0';  /* clip off last '\r' char */


    for(i = 0; i < (INTERP_MAX_PARAMS +1); i++)  /* including param0:command_ID */
        cmd_parms.argum[i] = NULL;

    /* jump over leading spaces */
    cmd_index = 0;  // do not use skip_spaces to save code

    if(cmd_index >= SZ_CMDBUF)
        return(ERR_PARSE);

    /* setup pointer to command (argument 0)*/
    cmd_parms.argum[parms_index] = cmd_buf + cmd_index;
    parms_index++;

    /* find end of command_ID*/
    for(;;)
    {
        cmdp = &cmd_buf[cmd_index];
        switch(*cmdp)
        {
            case '\0':
            case '\r':
            case '\n':
                *cmdp = '\0';   // Mark the end os string
                end_cmd_flag = 1;
                break;

            case ',':      /* first argument separator */
            //case '(':
                more_argum = 1;
                *cmdp = '\0';
                break;

            //case ' ':       /* 161193 - strip trailing spaces of token*/
            //    cmd_buf[cmd_index] = '\0';
            //    break;
        }

        if(more_argum || end_cmd_flag)
        {
            break;
        }
        else
        {
            cmd_index++;
            if(cmd_index > CMDBUFLEN)
                return(ERR_PARSE);
        }
    }

    if(!end_cmd_flag)           /* more params to scan?*/
    {
        /* jump over spaces, if any*/
        //cmd_index = skip_spaces(cmd_buf, cmd_index+1);
        cmd_index++;
        if(cmd_index >= CMDBUFLEN)
        {
            return(ERR_PARSE);
        }

        /* point to 1st argument*/
        cmd_parms.argum[parms_index] = cmd_buf + cmd_index;
        parms_index++;          /* ready for next params*/

        exit_flag   = 0;
        while(!exit_flag)
        {
            for(;;)
            {
                if(cmd_index >= CMDBUFLEN)
                    return(ERR_PARSE);

                cmdp = &cmd_buf[cmd_index];
                if(*cmdp == ',')
                {
                    *cmdp = '\0';

                    /* jump over spaces, if any*/
                    //cmd_index = skip_spaces(cmd_buf, cmd_index + 1);
                    cmd_index++;
                    if(cmd_index >= CMDBUFLEN)
                        return(ERR_PARSE);

                    /* point to next argument*/
                    cmd_parms.argum[parms_index] = cmd_buf + cmd_index;
                    if(++parms_index > INTERP_MAX_PARAMS +1)
                        exit_flag = 1;
                    break;
                }

                #ifdef USES_PARENTH
                if(*cmdp == ')')
                {
                    //cmd_buf[cmd_index] = '\0';
                    *cmdp = '\0';
                    exit_flag = 1;
                    break;
                }
                #endif

                if((*cmdp == '\0') ||
                 //(*cmdp == ' ' ) ||
                   (*cmdp == '\r') ||
                   (*cmdp == '\n')  )
                {
                    exit_flag = 1;
                    break;
                }

                cmd_index++;
                if(cmd_index > CMDBUFLEN)
                    return(ERR_PARSE);
            }
        }
    }
    parms_index--;              /* went too far..*/

    i = 0;
    while (strcmp(cmdtable[i].command, "END_T") !=0)
    {
        /* Care should be taken with command upper/lower case !!! */
        if(strcmp(cmd_parms.argum[0], cmdtable[i].command) == 0)   // found?
        {
            /*
            error_msg = (cmdtable[i].pfunction) ( parms_index,
                                    cmd_parms.argum[1],  cmd_parms.argum[2],
                                    cmd_parms.argum[3],  cmd_parms.argum[4],
                                    cmd_parms.argum[5],  cmd_parms.argum[6],
                                    cmd_parms.argum[7],  cmd_parms.argum[8],
                                    cmd_parms.argum[9],  cmd_parms.argum[10],
                                    cmd_parms.argum[11], cmd_parms.argum[12],
                                    cmd_parms.argum[13], cmd_parms.argum[14],
                                    cmd_parms.argum[15], cmd_parms.argum[16] );
            */
            cmd_parms.numarg = parms_index;
            error_msg = (cmdtable[i].pfunction) (&cmd_parms);

            switch (error_msg)
            {
                case EXEC_OK:
                    return(EXEC_OK);          /* exec OK*/

                case ERR_EXEC:
                    return(ERR_EXEC);         /* exec error*/

                default:
                    return(error_msg);         /* script param error */
            }
        }
        i++;
    }

    return(ERR_PARSE);                         /* parse error*/
}


/////////////////////////////////////////////////////////////////////////////////////////



/*----------------------------------------------------------------------*/
/* special implementation for xxx : no spaces is acceptable because of
   complicated 75 char in data stream implementation */
/*----------------------------------------------------------------------*/

#ifdef DOSKIPSPACE

int skip_spaces(char *string, int index)
{
    while(string[index] == ' ')
    {
        if(index >= CMDBUFLEN)
            break;
        index++;
    }
    return(index);
}

#endif

//---------------------------------------------------------------------------
/** @brief
    @param  None
    @return None
*/


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/** @} (end addtogroup 1) */
/** @} (end addtogroup 0) */

/******************* (C) COPYRIGHT RADICOS Technologies GmbH   **************/
/******************* (C) COPYRIGHT V-Engineering ****************************/
/******************* (C) COPYRIGHT L&V Design ********* END OF FILE *********/

