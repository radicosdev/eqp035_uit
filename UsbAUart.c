/*
** Filename: UsbAUart.c
**
** Automatically created by Application Wizard 1.4.2
**
** Part of solution Vinco_USBSlaveFT232AW in project Vinco_USBSlaveFT232AW
**
** Comments:
**
** Important: Sections between markers "FTDI:S*" and "FTDI:E*" will be overwritten by
** the Application Wizard
*/
#include "EQP035.h"

#include "USBSlaveFT232.h"

vos_tcb_t *tcbLoopback;

VOS_HANDLE hA = NULL;
VOS_HANDLE hFT232;

common_ioctl_cb_t iocb;
unsigned short bytesTransferred;

uint8 readbuf[128];


void loop(void)
{
	iocb.ioctl_code = VOS_IOCTL_COMMON_GET_RX_QUEUE_STATUS;
	iocb.get.queue_stat = 0;
	vos_dev_ioctl(hFT232, &iocb);

	if (iocb.get.queue_stat == 0)
	{
		vos_delay_msecs(100);
	}
	else
	{
		vos_dev_read(hFT232, &readbuf[0], iocb.get.queue_stat, &bytesTransferred);
		vos_dev_write(hFT232, &readbuf[0], iocb.get.queue_stat, &bytesTransferred);
	}
}
