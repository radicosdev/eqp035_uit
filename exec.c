/* ----------------------------------------------------------------------------
*******************************************************************************
**                                                                           **
**  Unpublished - all rights reserved under the copyright laws of Switzer-   **
**  land and all other countries.                                            **
**                                                                           **
**  Copyright (c) RADICOS Technologies GmbH, Austria                         **
**  Copyright (c) L&V Design, Switzerland                                    **
**  Copyright (c) V-Engineering, Switzerland                                 **
**  All Rights Reserved                                                      **
**  Licensed Material -- Property of Radicos/V-Engineering/L&V DESIGN        **
**                                                                           **
*******************************************************************************
---------------------------------------------------------------------------- */

/**
    @file
    @brief
        tbd
    @par Purpose of this file
    @par Implementation :
          TBD
    @par Usage :
    @version
    @date
*/

/******************************************************************************
 * @addtogroup ______
 * @{
 *      @addtogroup xxxxxx
 *      @brief
 *      @{
 ******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "interp.h"
#include "stdio.h"
#include "EQP035.h"
#include "gpioctrl.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Global variables to be exported , check in xxxx.h -------------------------*/
/* Private variables ---------------------------------------------------------*/

     // Serial working buffer
uint8 Exec_AnswerStr[CMDBUFLEN];


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/*----------------------------------------------------------------------*/

//---------------------------------------------------------------------------
/** @brief
    @param  None
    @return None
*/

void exec_send_OkHeader (PARAMS_T *pP)
{
    printf((char*)Exec_AnswerStr,"0,%s",pP->argum[0]);
    //Puts_1(Exec_AnswerStr);
}



//---------------------------------------------------------------------------
/** @brief
    @param  None
    @return None
*/

int16 CmdDisplayVersion (PARAMS_T *pP)
{
    
	sprintf((char*)Exec_AnswerStr,"0,RAdicos-VNC2 2022 02 09\r");
    Puts_1(Exec_AnswerStr);

    return NO_ERRCODE;
}

//---------------------------------------------------------------------------
/** @brief  Used set the mux to connect the defined node
    @param  The node to select
    @return ErrorCode
*/

int16 CmdNodeSelect(PARAMS_T *pP)
{
    uint8 *pV;
	uint8 NodeToSelect;
	uint8 ToOutput;
	int16 errcode;

    pV = pP->argum[1];
    NodeToSelect = atoi((const char*)pV);
	if ((NodeToSelect >= 1) && (NodeToSelect <= 32))
	{
		ToOutput = (NodeToSelect-1) | 0x80; //Do not nable now
		errcode = vos_gpio_write_port(GPIO_PORT_B, ToOutput);
		errcode = vos_gpio_write_pin(GPIO_B_6, 1);	//Latch
		errcode = vos_gpio_write_pin(GPIO_B_7, 0);	//Enable
	}
	else
	{
		return ERR_ARG;
	}

    sprintf((char*)Exec_AnswerStr,"0,Node connected = %d\r",NodeToSelect);
    Puts_1(Exec_AnswerStr);
    return NO_ERRCODE;
}

//---------------------------------------------------------------------------
/** @brief
    @param  None
    @return None
*/

int16 CmdDisableAll(PARAMS_T *pP)
{
	vos_gpio_write_port(GPIO_PORT_B, 0xff);
    sprintf((char*)Exec_AnswerStr,"0,Node disabled\r");
    Puts_1(Exec_AnswerStr);
    return NO_ERRCODE;
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/** @} (end addtogroup 1) */
/** @} (end addtogroup 0) */

/******************* (C) COPYRIGHT RADICOS Technologies GmbH   **************/
/******************* (C) COPYRIGHT V-Engineering ****************************/
/******************* (C) COPYRIGHT L&V Design ********* END OF FILE *********/


