/*
** Filename: EQP035.h
**
** Automatically created by Application Wizard 2.0.2
** 
** Part of solution UIT in project EQP035
**
** Comments: 
**
** Important: Sections between markers "FTDI:S*" and "FTDI:E*" will be overwritten by
** the Application Wizard
*/

#ifndef _EQP035_H_
#define _EQP035_H_

#include "vos.h"

/* FTDI:SHF Header Files */
#include "USB.h"
#include "USBSlave.h"
#include "ioctl.h"
#include "UART.h"
#include "SPIMaster.h"
#include "GPIO.h"
#include "USBSlaveFT232.h"
#include "stdio.h"
#include "errno.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
/* FTDI:EHF */

#include "interp.h"

/* FTDI:SDC Driver Constants */
#define VOS_DEV_USBSLAVE_1 0
#define VOS_DEV_USBSLAVE_2 1
#define VOS_DEV_UART 2
#define VOS_DEV_SPI_MASTER 3
#define VOS_DEV_GPIO_PORT_A 4
#define VOS_DEV_GPIO_PORT_B 5
#define VOS_DEV_USBSLAVE_FT232_1 6
#define VOS_DEV_USBSLAVE_FT232_2 7

#define VOS_NUMBER_DEVICES 8
/* FTDI:EDC */

/* FTDI:SXH Externs */
/* FTDI:EXH */

void Puts_1(uint8 *TheAnswer);

#endif /* _EQP035_H_ */
