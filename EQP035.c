/*
** Filename: EQP035.c
**
** Automatically created by Application Wizard 2.0.2
** 
** Part of solution UIT in project EQP035
**
** Comments: 
**
** Important: Sections between markers "FTDI:S*" and "FTDI:E*" will be overwritten by
** the Application Wizard
*/

#include "EQP035.h"

/* FTDI:STP Thread Prototypes */
vos_tcb_t *tcbMUXSETUP;
vos_tcb_t *tcbUSBUARTCOMM;

void MuxSetup();
void UsbUartComm();
/* FTDI:ETP */

/* FTDI:SDH Driver Handles */
VOS_HANDLE hUSBSLAVE_1; // USB Slave Port 1
VOS_HANDLE hUSBSLAVE_2; // USB Slave Port 2
VOS_HANDLE hUART; // UART Interface Driver
VOS_HANDLE hSPI_MASTER; // SPIMaster Interface Driver
VOS_HANDLE hGPIO_PORT_A; // GPIO Port A Driver
VOS_HANDLE hGPIO_PORT_B; // GPIO Port B Driver
VOS_HANDLE hUSBSLAVE_FT232_1; // Emulates an FT232 device using the USB Slave Interface
VOS_HANDLE hUSBSLAVE_FT232_2; // Emulates an FT232 device using the USB Slave Interface
/* FTDI:EDH */

/* Declaration for IOMUx setup function */
void iomux_setup(void);

/* Main code - entry point to firmware */
void main(void)
{
int16 errcode;
	/* FTDI:SDD Driver Declarations */
	// UART Driver configuration context
	uart_context_t uartContext;
	// SPI Master configuration context
	spimaster_context_t spimContext;
	// GPIO Port A configuration context
	gpio_context_t gpioContextA;
	// GPIO Port B configuration context
	gpio_context_t gpioContextB;
	// USB Slave FT232 configuration context
	usbSlaveFt232_init_t usbslaveFT232Context;
	// USB Slave FT232 configuration context
	//usbSlaveFt232_init_t usbslaveFT232Context;
	/* FTDI:EDD */

	/* FTDI:SKI Kernel Initialisation */
	vos_init(50, VOS_TICK_INTERVAL, VOS_NUMBER_DEVICES);
	vos_set_clock_frequency(VOS_48MHZ_CLOCK_FREQUENCY);
	vos_set_idle_thread_tcb_size(512);
	/* FTDI:EKI */

	iomux_setup();

	/* FTDI:SDI Driver Initialisation */
	// Initialise USB Slave Port 0
	usbslave_init(0, VOS_DEV_USBSLAVE_1);
	
	// Initialise USB Slave Port 1
	usbslave_init(1, VOS_DEV_USBSLAVE_2);
	
	// Initialise UART
	uartContext.buffer_size = VOS_BUFFER_SIZE_128_BYTES;
	uart_init(VOS_DEV_UART,&uartContext);
	
	// Initialise SPI Master
	spimContext.buffer_size = VOS_BUFFER_SIZE_128_BYTES;
	spimaster_init(VOS_DEV_SPI_MASTER,&spimContext);
	
	// Initialise GPIO A
	gpioContextA.port_identifier = GPIO_PORT_A;
	gpio_init(VOS_DEV_GPIO_PORT_A,&gpioContextA);
	
	// Initialise GPIO B
	gpioContextB.port_identifier = GPIO_PORT_B;
	gpio_init(VOS_DEV_GPIO_PORT_B,&gpioContextB);
	
	// Initialise USB Slave FT232 Driver
	usbslaveFT232Context.in_ep_buffer_len = 128;
	usbslaveFT232Context.out_ep_buffer_len = 128;
	usbslaveft232_init(VOS_DEV_USBSLAVE_FT232_1, &usbslaveFT232Context);
	
	// Initialise USB Slave FT232 Driver
	usbslaveFT232Context.in_ep_buffer_len = 128;
	usbslaveFT232Context.out_ep_buffer_len = 128;
	usbslaveft232_init(VOS_DEV_USBSLAVE_FT232_2, &usbslaveFT232Context);
	
	
	
	
	
	/* FTDI:EDI */

	/* FTDI:SCT Thread Creation */
	tcbMUXSETUP = vos_create_thread_ex(20, 2048, MuxSetup, "MuxControl", 0);
	tcbUSBUARTCOMM = vos_create_thread_ex(24, 2048, UsbUartComm, "UsbToUart", 0);
	/* FTDI:ECT */

	vos_start_scheduler();

main_loop:
	goto main_loop;
}

/* FTDI:SSP Support Functions */

unsigned char usbslave_connect(VOS_HANDLE hUSB)
{
    usbslave_ioctl_cb_t iocb;
    unsigned char ret;

    iocb.ioctl_code = VOS_IOCTL_USBSLAVE_CONNECT;
    iocb.set = (void *) 0;
    ret = vos_dev_ioctl(hUSB, &iocb);
    
    return ret;
}

unsigned char usbslave_disconnect(VOS_HANDLE hUSB)
{
    usbslave_ioctl_cb_t iocb;
    unsigned char ret;

    iocb.ioctl_code = VOS_IOCTL_USBSLAVE_DISCONNECT;
    iocb.set = (void *) 0;
    vos_dev_ioctl(hUSB, &iocb);

    return ret;
}


VOS_HANDLE ft232_slave_attach(VOS_HANDLE hUSB, unsigned char devSlaveFT232)
{
	common_ioctl_cb_t ft232_iocb;
	VOS_HANDLE hSlaveFT232;

	// open FT232 driver
	hSlaveFT232 = vos_dev_open(devSlaveFT232);

	// attach FT232 to USB Slave port
	ft232_iocb.ioctl_code = VOS_IOCTL_USBSLAVEFT232_ATTACH;
	ft232_iocb.set.data   = (void*)hUSB;
	if (vos_dev_ioctl(hSlaveFT232, &ft232_iocb) != USBSLAVE_OK)
	{
		vos_dev_close(hSlaveFT232);
		hSlaveFT232 = NULL;
	}

	return hSlaveFT232;
}

void ft232_slave_detach(VOS_HANDLE hSlaveFT232)
{
	common_ioctl_cb_t ft232_iocb;

	if (hSlaveFT232)
	{
		ft232_iocb.ioctl_code = VOS_IOCTL_USBSLAVEFT232_DETACH;

		vos_dev_ioctl(hSlaveFT232, &ft232_iocb);
		vos_dev_close(hSlaveFT232);
	}
}

/* FTDI:ESP */

void open_drivers(void)
{
        /* Code for opening and closing drivers - move to required places in Application Threads */
        /* FTDI:SDA Driver Open */
        hUSBSLAVE_1 = vos_dev_open(VOS_DEV_USBSLAVE_1);
        hUSBSLAVE_2 = vos_dev_open(VOS_DEV_USBSLAVE_2);
        hUART = vos_dev_open(VOS_DEV_UART);
        hSPI_MASTER = vos_dev_open(VOS_DEV_SPI_MASTER);
        hGPIO_PORT_A = vos_dev_open(VOS_DEV_GPIO_PORT_A);
        hGPIO_PORT_B = vos_dev_open(VOS_DEV_GPIO_PORT_B);
        /* FTDI:EDA */
}

void attach_drivers(void)
{
        /* FTDI:SUA Layered Driver Attach Function Calls */
        hUSBSLAVE_FT232_1 = ft232_slave_attach(hUSBSLAVE_1, VOS_DEV_USBSLAVE_FT232_1);
        hUSBSLAVE_FT232_2 = ft232_slave_attach(hUSBSLAVE_2, VOS_DEV_USBSLAVE_FT232_2);
        // TODO attach stdio to file system and stdio interface
        //fsAttach(hFAT_FILE_SYSTEM); // VOS_HANDLE for file system (typically FAT)
        //stdioAttach(hUART); // VOS_HANDLE for stdio interface (typically UART)
        /* FTDI:EUA */
}

void close_drivers(void)
{
        /* FTDI:SDB Driver Close */
        vos_dev_close(hUSBSLAVE_1);
        vos_dev_close(hUSBSLAVE_2);
        vos_dev_close(hUART);
        vos_dev_close(hSPI_MASTER);
        vos_dev_close(hGPIO_PORT_A);
        vos_dev_close(hGPIO_PORT_B);
        /* FTDI:EDB */
}

/* Application Threads */

//Thread to make the communication between USB and Rx/Tx on a selected node
//Just transfert datas in each direction USBtoUART and UARTtoUSB
void UsbUartComm()
{
common_ioctl_cb_t iocb, uart_iocb ;
unsigned short bytesTransferred;
uint8 readbuf[128];
uint8 writebuf[128];

	/* Thread code to be added here */
	/* FTDI:SDD Driver Declarations */
	// UART Driver configuration context
	uart_context_t uartContext;
	// SPI Master configuration context
	spimaster_context_t spimContext;
	// GPIO Port A configuration context
	gpio_context_t gpioContextA;
	// GPIO Port B configuration context
	gpio_context_t gpioContextB;
	// USB Slave FT232 configuration context
	usbSlaveFt232_init_t usbslaveFT232Context;
	// USB Slave FT232 configuration context
	//usbSlaveFt232_init_t usbslaveFT232Context;
	/* FTDI:EDD */

	// open USB Slave
	hUSBSLAVE_2 = vos_dev_open(VOS_DEV_USBSLAVE_2);
	// open & attach
	hUSBSLAVE_FT232_2 = ft232_slave_attach(hUSBSLAVE_2, VOS_DEV_USBSLAVE_FT232_2);
	//setup();

	// open UART
	hUART = vos_dev_open(VOS_DEV_UART);
	uart_iocb.ioctl_code = VOS_IOCTL_UART_SET_BAUD_RATE;
	uart_iocb.set.uart_baud_rate = UART_BAUD_115200;
	vos_dev_ioctl(hUART,&uart_iocb);
	uart_iocb.ioctl_code = VOS_IOCTL_UART_SET_FLOW_CONTROL;
	uart_iocb.set.param = UART_FLOW_NONE;
	vos_dev_ioctl(hUART,&uart_iocb);
	uart_iocb.ioctl_code = VOS_IOCTL_UART_SET_DATA_BITS;
	uart_iocb.set.param = UART_DATA_BITS_8;
	vos_dev_ioctl(hUART,&uart_iocb);
	uart_iocb.ioctl_code = VOS_IOCTL_UART_SET_STOP_BITS;
	uart_iocb.set.param = UART_STOP_BITS_1;
	vos_dev_ioctl(hUART,&uart_iocb);
	uart_iocb.ioctl_code = VOS_IOCTL_UART_SET_PARITY;
	uart_iocb.set.param = UART_PARITY_NONE;
	vos_dev_ioctl(hUART,&uart_iocb);	

	//Without DMA, unable to have a reliable communication at 115200baud
	uart_iocb.ioctl_code = VOS_IOCTL_COMMON_ENABLE_DMA;
	uart_iocb.set = DMA_ACQUIRE_AS_REQUIRED;
	vos_dev_ioctl(hUART,&uart_iocb);
	
	while (1)
	{
		//UARTtoUSB
		iocb.ioctl_code = VOS_IOCTL_COMMON_GET_RX_QUEUE_STATUS;
		iocb.get.queue_stat = 0;
		vos_dev_ioctl(hUART, &iocb);

		if (iocb.get.queue_stat != 0)
		{
			vos_dev_read(hUART, &writebuf[0], iocb.get.queue_stat, &bytesTransferred);
			vos_dev_write(hUSBSLAVE_FT232_2, &writebuf[0], iocb.get.queue_stat, &bytesTransferred);
		}
		//USBtoUART
		iocb.ioctl_code = VOS_IOCTL_COMMON_GET_RX_QUEUE_STATUS;
		iocb.get.queue_stat = 0;
		vos_dev_ioctl(hUSBSLAVE_FT232_2, &iocb);

		if (iocb.get.queue_stat != 0)
		{
			vos_dev_read(hUSBSLAVE_FT232_2, &readbuf[0], iocb.get.queue_stat, &bytesTransferred);
			vos_dev_write(hUART, &readbuf[0], iocb.get.queue_stat, &bytesTransferred);
		}
		vos_delay_msecs(100);
	}
}

	
void Puts_1(uint8 *TheAnswer)
{
unsigned short bytesTransferred;
	
	vos_dev_write(hUSBSLAVE_FT232_1, TheAnswer, strlen(TheAnswer), &bytesTransferred);
	
}
	
//Thread to interpret command given on the other USB port
//1- define commands for: mux connection to connect/unconnect a node Rx/Tx to the USB (UsbUartComm())
//2- define utility commands
//3- in prevision integrate an SPI connection to an AD converter to include temperature references measurement
//Cmd traitment imported from ST8 (AMM) firmware
void MuxSetup()
{
common_ioctl_cb_t iocb, uart_iocb ;

unsigned short bytesTransferred;
uint8 CmdLine[128];
uint8 Exec_AnswerStr[128];
uint8 i;
int16 errcode;
	/* Thread code to be added here */
	// GPIO Port A configuration context
	gpio_context_t gpioContextA;
	// GPIO Port B configuration context
	gpio_context_t gpioContextB;
	// USB Slave FT232 configuration context
	usbSlaveFt232_init_t usbslaveFT232Context;
	// open USB Slave
	hUSBSLAVE_1 = vos_dev_open(VOS_DEV_USBSLAVE_1);
	// open & attach
	hUSBSLAVE_FT232_1 = ft232_slave_attach(hUSBSLAVE_1, VOS_DEV_USBSLAVE_FT232_1);
	//setup();

	errcode = vos_gpio_set_port_mode(GPIO_PORT_B, 0xff);
	errcode = vos_gpio_write_port(GPIO_PORT_B, 0xff);
	
	if (errcode != GPIO_OK)
	{
		sprintf((char*)Exec_AnswerStr,"Gpio error code =%d\r",errcode);
		Puts_1(Exec_AnswerStr);
	}

	//! do not use it = to slow PW 20220210
	//printf direct to 
	//stdioAttach(hUSBSLAVE_FT232_1);
	//printf("RAdicos-VNC2 2022 02 09\r");

	while (1)
	{
		iocb.ioctl_code = VOS_IOCTL_COMMON_GET_RX_QUEUE_STATUS;
		iocb.get.queue_stat = 0;
		vos_dev_ioctl(hUSBSLAVE_FT232_1, &iocb);

		if (iocb.get.queue_stat == 0)
		{
			vos_delay_msecs(100);
		}
		else
		{
			vos_dev_read(hUSBSLAVE_FT232_1, &CmdLine[0], iocb.get.queue_stat, &bytesTransferred);
			
			//In upper
			for(i=0;i<bytesTransferred;i++)
			{
				if ( (CmdLine[i] >= 'a') && (CmdLine[i] <= 'z')) CmdLine[i] &= ~0x20;
			}
			/* here only numerical form is send back with <cr> */
			if ( (errcode = parse((char*)CmdLine)) != (int16)NO_ERRCODE )
			{
				printf((char*)Exec_AnswerStr,"%d\r",errcode);
				Puts_1(Exec_AnswerStr);
			}
		}
	
	}


}

